function $_id(id) {
    return document.getElementById(id);
}

function panel1() {
    $_id('main').style.display = 'none';
    $_id('panel1').style.display = 'block';
}

function panel2() {
    $_id('main').style.display = 'none';
    $_id('panel2').style.display = 'block';
}
